from typing import Dict


from src.core.database.redis_connection import RedisTable
from src.core.utils.io import jsonio
from config import (
    ENDPOINT_REDIS
)


class SubscriberStore:
    def __init__(self):
        self.subscriber_db = ENDPOINT_REDIS.get("forward_name_db")
        self.red = RedisTable(config=ENDPOINT_REDIS, table="subscriber")

    def get_data(self, subscriber_id) -> Dict:
        if not self.subscriber_db:
            return {}
        value = self.red.get(subscriber_id, self.subscriber_db)
        if not value:
            return {}
        return jsonio.loads_utf8(value)

    def save_data(self, subscriber_id, data: Dict) -> None:
        if not self.subscriber_db:
            return
        data = jsonio.dumps_utf8(data)
        self.red.set(subscriber_id, data)
