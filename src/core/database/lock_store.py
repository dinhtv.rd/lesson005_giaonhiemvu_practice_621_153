from typing import Text, Optional


from rasa.core.lock import TicketLock
from rasa.core.lock_store import LockStore

from src.core.database.redis_connection import RedisTable
from src.core.utils.io import jsonio
from config import (
    ENDPOINT_REDIS
)


class RedisLockStore(LockStore):
    """Redis store for ticket locks dev by Smartcall."""

    def __init__(
            self,
            endpoint_config=None
    ):
        self.red = RedisTable(config=ENDPOINT_REDIS, table="lock")
        super().__init__()

    def get_lock(self, conversation_id: Text) -> Optional[TicketLock]:
        serialised_lock = self.red.get(conversation_id)
        if serialised_lock:
            return TicketLock.from_dict(jsonio.loads_utf8(serialised_lock))

    def delete_lock(self, conversation_id: Text) -> None:
        deletion_successful = self.red.delete(conversation_id)
        self._log_deletion(conversation_id, deletion_successful)

    def save_lock(self, lock: TicketLock) -> None:
        self.red.set(lock.conversation_id, lock.dumps())
