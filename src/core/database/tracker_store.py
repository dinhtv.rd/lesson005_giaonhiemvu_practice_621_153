import logging
from typing import Optional, Text, Iterable
import time
import copy

from rasa.shared.core.trackers import DialogueStateTracker
from rasa.core.tracker_store import TrackerStore
from rasa.core.brokers.broker import EventBroker

from config import (
    ENDPOINT_DATABASE,
    ENDPOINT_REDIS,
    CONFIG_DOMAIN,
    DOMAIN_FINISH_ACTIONS,
    DB_KEY_HISTORY
)
from src.core.database.database_connection import DatabaseConnection

from src.core.database.context_store import RedisStore
from src.core.database.redis_connection import RedisTable

logger = logging.getLogger(__name__)


class SQLTrackerStoreCustom(TrackerStore):
    """Store which can save and retrieve trackers from an SQL database."""

    def __init__(
            self,
            domain,
            host="localhost",
            port=6379,
            db=0,
            password: Optional[Text] = None,
            event_broker: Optional[EventBroker] = None,
            record_exp: Optional[float] = None,
            use_ssl: bool = False,
    ):
        self.finish_actions = CONFIG_DOMAIN.get(DOMAIN_FINISH_ACTIONS)
        self.redis_more_store = RedisStore()
        self.connect = DatabaseConnection(host=ENDPOINT_DATABASE.get("host"),
                                          username=ENDPOINT_DATABASE.get("username"),
                                          password=ENDPOINT_DATABASE.get("password"),
                                          db=ENDPOINT_DATABASE.get("db"),
                                          port=ENDPOINT_DATABASE.get("port"))
        self.history_table = ENDPOINT_DATABASE.get(DB_KEY_HISTORY)

        self.red = RedisTable(config=ENDPOINT_REDIS, table="tracker")

        self.record_exp = record_exp
        logger.info("[TRACKER STORE] Create Custom tracker store successful")

        super().__init__(domain, event_broker)

    def insert_history(self, sender_id, pre_action, timestamp, intent_name, action_name,
                       text_user, text_bot, entities):
        sql = "INSERT INTO "
        sql += self.history_table + " (sender_id, pre_action, timestamp, intent_name, action_name,"
        sql += " text_user, text_bot, entities) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        val = (sender_id, pre_action, timestamp, intent_name, action_name,
               text_user, text_bot, entities)
        connection = self.connect.get_connection()
        cursor = connection.cursor()
        cursor.execute(sql, val)
        connection.commit()
        cursor.close()
        connection.close()

    def keys(self) -> Iterable[Text]:
        """Return keys of the Redis Tracker Store"""
        return self.red.keys()

    def retrieve(self, sender_id: Text) -> Optional[DialogueStateTracker]:
        """Create a tracker from all previously stored events."""
        stored = self.red.get(sender_id)
        if stored is not None:
            return self.deserialise_tracker(sender_id, stored)
        else:
            return None

    def save(self, tracker: DialogueStateTracker, timeout=None) -> None:
        """Update database with events from the current conversation."""
        sender_id = tracker.sender_id
        if self.event_broker:
            self.stream_events(tracker)
        if not timeout and self.record_exp:
            timeout = self.record_exp
        serialised_tracker = self.serialise_tracker(tracker)
        self.red.set(sender_id, serialised_tracker, ex=timeout)
        logger.info(
            f"[TRACKER STORE][{sender_id}] - Tracker stored to Redis db".format(sender_id)
        )

        # Delete recorder in redis db if conversation was over
        recorder = self.redis_more_store.get_data(sender_id).tickets["recorder"]
        entities = recorder.get_entities_dumped()
        if recorder.pre_action is None:
            pre_action = "START"
        else:
            pre_action = copy.deepcopy(recorder.pre_action)
        if recorder.cur_action in self.finish_actions:
            self.redis_more_store.delete(sender_id)
            self.red.delete(sender_id)

        # Get a pair of messages include pre_action, intent, cur_action, text_bot, text_user
        intent_real = None
        action_real = None
        text_user = None
        text_bot = None
        events = tracker.events
        for event in events:
            data = event.as_dict()
            talker = data.get('event')
            if talker == "user":
                text_user = data.get("text")
            if talker == "bot":
                text_bot = data.get("text")
            intent = data.get("parse_data", {}).get("intent", {}).get("name")
            action = data.get("name")
            if action is not None and action != "action_listen":
                action_real = action
            if intent is not None:
                intent_real = intent

        # Insert a pair of messages to history which use for view log called conversation
        timestamp = time.time()
        if intent_real is not None and action_real is not None and text_bot is not None and text_user is not None:
            # insert data into database mysql
            try:
                self.insert_history(sender_id, pre_action, timestamp, intent_real, action_real,
                                    text_user, text_bot, entities)
                logger.info(f"[TRACKER STORE][{sender_id}] - Insert a pair of messages to db.history successful "
                            f"{'='*100}")
            except Exception as e:
                import traceback
                logger.info(f"[TRACKER STORE][{sender_id}] - Cannot insert a pair of messages to db.history "
                            f"- follow this error: {e}")
                logger.info(traceback.format_exc())
