import logging

from src.core.utils.io import jsonio

logger = logging.getLogger(__name__)


async def insert(connect, testcase_table, testcase):
    keys = []
    values = []
    tmp_values = []
    for key in testcase:
        value = testcase.get(key)
        if value is not None and key not in ["testcase_id"]:
            keys.append(key)
            if key in ["input_slots", "conversation", "report", "testing_result"]:
                values.append(jsonio.dumps_utf8(value))
            else:
                values.append(str(value))
            tmp_values.append("%s")
    keys = ",".join(keys)
    tmp_keys = ",".join(tmp_values)

    sql = f"INSERT INTO {testcase_table} " \
          f"({keys}) VALUES ({tmp_keys})"

    connection = connect.get_connection()
    cursor = connection.cursor()
    cursor.execute(sql, tuple(values))
    testcase_id = cursor.lastrowid
    connection.commit()
    cursor.close()
    connection.close()
    return {
        "status": 0,
        "msg": "Success",
        "testcase_id": testcase_id
    }


async def select(connect, testcase_table, testcase_id=None):
    connection = connect.get_connection()
    cursor = connection.cursor()
    connection.commit()
    if testcase_id:
        sql = f"SELECT testcase_id, input_slots, conversation, report, description, testing_result " \
              f"FROM {testcase_table} " \
              f"WHERE testcase_id = %s"
        cursor.execute(sql, [testcase_id])
    else:
        sql = f"SELECT testcase_id, input_slots, conversation, report, description, testing_result " \
              f"FROM {testcase_table} "
        cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    connection.close()

    list_testcases = []
    for row in rows:
        testcase_id, input_slots, conversation, report, description, testing_result = \
            row[0], row[1], row[2], row[3], row[4], row[5]
        input_slots = jsonio.loads_utf8(str(input_slots)) if input_slots else None
        conversation = jsonio.loads_utf8(str(conversation)) if conversation else None
        report = jsonio.loads_utf8(report) if report else None
        testing_result = jsonio.loads_utf8(testing_result) if testing_result else None
        list_testcases.append({
            "testcase_id": testcase_id,
            "input_slots": input_slots,
            "conversation": conversation,
            "report": report,
            "description": description,
            "testing_result": testing_result,
        })
    return {
        "status": 0,
        "msg": "Success",
        "testcases": list_testcases,
    }


async def update(connect, testcase_table, testcase):
    keys = []
    values = []
    for key in testcase:
        value = testcase.get(key)
        if value is not None and key not in ["testcase_id"]:
            keys.append(f"{key}=%s")
            if key in ["input_slots", "conversation", "report", "testing_result"]:
                values.append(jsonio.dumps_utf8(value).encode('utf8').decode())
            else:
                values.append(str(value))
    keys = ",".join(keys)
    testcase_id = testcase.get("testcase_id")

    sql = f"UPDATE {testcase_table} " \
          f"SET {keys} " \
          f"WHERE testcase_id='{testcase_id}'"

    connection = connect.get_connection()
    cursor = connection.cursor()
    cursor.execute(sql, tuple(values))
    connection.commit()
    cursor.close()
    connection.close()

    return {
        "status": 0,
        "msg": "Success",
        "testcase_id": testcase_id
    }


async def delete(connect, testcase_table, testcase_id=None):
    sql = f"DELETE FROM {testcase_table} " \
          f"WHERE testcase_id='{testcase_id}'"

    connection = connect.get_connection()
    cursor = connection.cursor()
    cursor.execute(sql)
    connection.commit()
    rowcount = cursor.rowcount
    if rowcount == 0:
        log = f"No testcase has testcase_id={testcase_id}, no testcase deleted"
    else:
        log = f"{rowcount} testcase deleted"
    cursor.close()
    connection.close()

    return {
        "status": 0,
        "msg": "Success",
        "log": log
    }
