import json


def loads_utf8(json_str):
    return json.loads(json_str)


def dumps_utf8(json_dict):
    return json.dumps(json_dict, ensure_ascii=False)


def load_utf8(json_file):
    return json.load(json_file)


def dump_utf8(json_str, json_file):
    return json.dump(json_str, json_file, ensure_ascii=False, indent=2)
