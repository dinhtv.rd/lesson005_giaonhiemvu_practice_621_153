def norm_name_class(name):
    if not name:
        return name
    name = name.replace("-", "__")
    return f"Action_{name}"