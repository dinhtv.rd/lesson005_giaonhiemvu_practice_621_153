def better(x, y):
    """
    Entity selection function
    Default: The choice to appear position and length priority
    Custom: The choice depends on the type of entity
    """

    # Default: position ahead & longer
    if x["end"] - 1 < y["start"]:
        return x
    if y["end"] - 1 < x["start"]:
        return y
    if len(x["value"]) > len(y["value"]):
        return x
    if len(y["value"]) > len(x["value"]):
        return y
    return y
