from src.core.condition.interfaces.impl_record import ImplUpdateGlobal


class GetEntities(ImplUpdateGlobal):
    def name(self):
        return "get_entities"

    def process(self, recorder, args):
        return self.record

    def __init__(self):
        super().__init__()
        self.record = {
        }
