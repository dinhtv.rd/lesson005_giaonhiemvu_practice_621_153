from src.core.condition.interfaces.impl_record import ImplUpdateGlobal


class GetDevLogger(ImplUpdateGlobal):
    def name(self):
        return "get_logger"

    def process(self, recorder, args):
        self.record["pre_action"] = recorder.pre_action
        self.record["cur_intent"] = recorder.cur_intent
        self.record["cur_action"] = recorder.cur_action
        self.record["processed_message"] = recorder.get_last_message()
        self.record["model_intent"] = recorder.model_intent
        self.record["metadata"] = recorder.get_last_metadata()
        self.record["entities"] = recorder.get_entities()
        return self.record

    def __init__(self):
        super().__init__()
        self.record = {
            "pre_action": None,
            "cur_intent": None,
            "cur_action": None,
            "processed_message": None,
            "model_intent": None,
            "metadata": None,
            "entities": {

            }
        }
