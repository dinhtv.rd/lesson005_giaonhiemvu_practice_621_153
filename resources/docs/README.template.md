# Document for making Template action

## File

```
config/template_action.json
```

## Form

* The json file contain a dict of actions, in which the key is a name of action
* Each action will have multiple responses to the master depending on the condition. Each action will have many ways of
  responding to the master depending on the condition. They are distinguished by `"properties"` as the condition. The
  value of `properties` is a dict with keys are record values ([Recorder & Records](README.recorder.md)), and the
  condition is an in equal term between the `key-value` and `record-value`
* `status`:
    * `CHAT`: normal action
    * `END`: finish action
    * `ACTION`: request action (wait for requesting)
    * `FORWARD`: action forward to other sip_phone_gw
* `text`: text response
* `text_tts`: text response in Text-to-speech form
* Example:

```json
{
  "action_start": [
    {
      "properties": {
        "check_repeat" : true
      },
      "status": "CHAT",
      "text": "Xin nhắc lại. thông báo tới số {customer_phone}. Anh chị đã trúng thưởng một chiếc kẹo mút trị giá 1 tỷ đồng.",
      "text_tts": "Xin nhắc lại. thông báo tới số {customer_phone}. Anh chị đã trúng thưởng một chiếc kẹo mút trị giá 1 tỷ đồng."
    },
    {
      "status": "CHAT",
      "text": "Xin thông báo tới số {customer_phone}. Anh chị đã trúng thưởng một chiếc kẹo mút trị giá 1 tỷ đồng.",
      "text_tts": "Xin thông báo tới số {customer_phone}. Anh chị đã trúng thưởng một chiếc kẹo mút trị giá 1 tỷ đồng."
    }
  ],
  "action_busy": [
    {
      "status": "END",
      "text": "Xin lỗi vì đã làm phiền, chào tạm biệt anh chị.",
      "text_tts": "Xin lỗi vì đã làm phiền, chào tạm biệt anh chị."
    }
  ],
  "action_quit": [
    {
      "status": "END",
      "text": "quit.",
      "text_tts": "quit."
    }
  ]
}
```